== Prefácio
=== Confidencialidade, direitos autorais e responsabilidade
Este é um documento orientado ao cliente entre a Red Hat ©, Inc. e o Nome do cliente. Copyright {year} Red Hat, Inc. Todos os direitos reservados. Nenhum dos trabalhos protegidos por direitos autorais neste documento pode ser reproduzido ou utilizado de qualquer forma ou por qualquer meio gráfico, eletrônico ou mecânico, incluindo fotocópias, gravações ou sistemas de recuperação de informações e armazenamento, sem a permissão por escrito da Red Hat, exceto quando necessário, a fim de compartilhar essas informações conforme pretendido com as partes confidenciais mencionadas acima.


=== Introdução
O programa Cloud Success Architecture (CSA) é um programa financiado pela CEE que fornece um compromisso flexível e de alto toque para ajudar os clientes na adoção das tecnologias de nuvem híbrida da Red Hat. O serviço é direcionado a clientes estratégicos que adquiriram assinaturas de produtos Red Hat Cloud: OpenStack, OpenShift Container Platform, Ansible ou CloudForms.

=== Sobre este documento
O objetivo deste documento é relatar os resultados da execução da certificação da plataforma instalada no ambiente {environment}.

=== Público
Este documento é destinado a administradores, arquitetos e desenvolvedores de sistemas {customer}.



=== Lista de verificação de nomenclatura
As imagens a seguir auxiliarão na revisão dos pontos de controle contemplados neste documento:

.Lista de verificação
[cols=2,cols="^1,<5",options=header]
|===
<|Imagen <|Explicación

|image:yes.png[] | Passe na revisão, não há comentários.
|image:warn.png[] | Há comentários sobre os resultados obtidos
|image:no.png[] | A revisão falha, as recomendações são entregues.

|===

<<<
=== Terminologia

.Terms Table
[cols=2,cols="1,5",options=header]
|===
<|Iniciais <|Definição

|OCP | Openshift Container Platform
|OCS | Openshift Container Storage
|CSA | Cloud Success Architect
|CEE | Customer Experience & Engagement
|API | Application Programming Interface
|DNS | Domain Name System
|DHCP| Dynamic Host Configuration Protocol
|FQDN| Fully Qualified Domain Name
|NTP | Network Time Protocol
|OS  | Operating System
|PV  | Persistent Volume
|RHEL| Red Hat Enterprise Linux

|===
