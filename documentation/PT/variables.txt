:subject: Revisão de Ambiente Openshift 4.x 
:subtitle: Preparado para 
:name:  your name
:email:  email@redhat.com
:date: date
:description:  Revisão do ambiente OCP
:doctype: book
:confidentiality: Confidencial
:customer:  Customer Name
:toclevels: 6
:nolang:
:numbered:
:chapter-label:
:lang: es
:icons: font
:pdf-page-size: A4
:pdf-style: redhat
:pdf-stylesdir: .
:revnumber: 1.0.0
:imagesdir: images

:environment: Producción
:year: 2021

:toc-title: Indice
:appendix-caption:
:caution-caption: Warning
:example-caption: Example
:figure-caption: Imagem
:table-caption: Table
:version-label: Versiones:
