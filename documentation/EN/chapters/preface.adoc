== Preface
=== Confidentiality, Copyright and Responsibility

This is a customer oriented document between Red Hat ©, Inc. and Customer Name. Copyright {year} Red Hat, Inc. All rights reserved. None of the work protected by copyright in this document may be reproduced or used in any way or by any graphic, electronic or mechanical means, including photocopying, recording, or storage and information retrieval systems, without Red Hat’s written permission except as required in order to share this information as intended with the confidential parties aforementioned.


=== Introduction
The Cloud Success Architecture (CSA) program is a CEE funded program that provides a flexible and high-touch engagement to assist customers with the adoption of Red Hat’s Hybrid Cloud Technologies.  The service is targeted towards strategic customers who have purchased subscriptions to Red Hat Cloud products:  OpenStack, OpenShift Container Platform, Ansible, or CloudForms.


=== About this document
The objective of this document is to report the results of the certification execution of the platform installed in the {environment} environment.


=== Audience
This document is intended for {customer} system administrators, architects, and developers.


=== Checklist Nomenclature
The following images will help in the review of the check points contemplated in this document:

.Nomenclature
[cols=2,cols="^1,<5",options=header]
|===
<|Imagen <|Explicación

|image:yes.png[] | Pass the review, there are no comments.
|image:warn.png[] | There are comments to be reviewed
|image:no.png[] | Fails the review, recommendations are given.

|===


<<<
=== Terminology

.Terms Table
[cols=2,cols="1,5",options=header]
|===
<|Term <|Definition

|OCP | Openshift Container Platform
|OCS | Openshift Container Storage
|CSA | Cloud Success Architect
|CEE | Customer Experience & Engagement
|API | Application Programming Interface
|DNS | Domain Name System
|DHCP| Dynamic Host Configuration Protocol
|FQDN| Fully Qualified Domain Name
|NTP | Network Time Protocol
|OS  | Operating System
|PV  | Persistent Volume
|RHEL| Red Hat Enterprise Linux

|===
