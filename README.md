# Red Hat Openshift Container Platform 4 - Review

This is a simple automated review for Red Hat Openshift Container Platform 4.

##  Environment


* Openshift 4 Demo workshop - Requested from [Red Hat Product Demo System](https://rhpds.redhat.com/)

   *Catalogs > Workshops > Openshift 4.4 Workshop > Order*

* asciidoctor tools must be installed:

  1. [https://asciidoctor.org/docs/install-toolchain/](https://asciidoctor.org/docs/install-toolchain/)
  2. [https://asciidoctor.org/docs/asciidoctor-pdf/](https://asciidoctor.org/docs/asciidoctor-pdf/)

* Ansible 2.9.1 & Python 3.7.5

## Usage

1.- Clone this repository on your local machine

2.- Change bastion hostname and optionally provide the bastion user and ssh key into hosts file.

```
[bastion]
bastion.example.com

[bastion:vars]
ansible_user=root
# ansible_ssh_private_key_file=/path/to/.ssh/id_rsa
```
Note: This instruction assumes you have passwordless access. If not, you have to add ansible_password to hosts file.

3.- Execute playbook using following command:
```
# cd ocp4-review/
# sudo ansible-playbook -i hosts healthcheck.yml
```

4.- This playbook will create templates in 3 languages. You can find it into document folder as follows:

* EN: English
* ES: Spanish
* PT: Portuguese

When playbook finishes, you'll have 2 important set of files into documentation/{LANG}/chapters/appendix folder:
* /data: All command output (by task groups)
* /checklist: checklist structure (by task groups)

5.- Before to generate the final document you need to do some changes:

* documentation/{LANG}/variables.txt:
  1. csa: *your name*
  2. email: *your email*
  3. date: *actual date*
  4. customer_name: *customer name*

* documentation/redhat-theme.yml

   Change the fonts location on your local environment.  

* documentation/{LANG}/chapters/versions.adoc:

   Set here your document version:
   
```
> === Versions
>
>[cols=4,cols="1,2,3,4",options=header]
>|===
>|Version
>|Date
>|Author
>|Changes

|<Put here the version number (1.0, 1.1, etc)>
|<Put here date>
|{csa} <{email}>
|<Put here the changes done>.
|===
```

* documentation/images: Replace architecture.png by your own environment diagram
   

* documentation/{LANG}/chapters/architecture.adoc:

  Replace *put here your architecture general comments* by your own comments about customer architecture. :P
   
* documentation/{LANG/}chapters/appendix/checklist/*.adoc

   Make the analysis on data collected in documentation/chapters/appendix/data folder and update all checklist docs using custom images:

   1. yes.png
   2. no.png
   3. warn.png (by default)

   Structure
   ```
	    | <id>
        | <image result>
        | <task name> (it doesn't be changed)
        | <your comments>

	```

   Example:
	```
	    | 2
        | image:yes.png[]
        | A shared network must exist between the masters and the nodes.
        | All nodes are Running and Ready

        | 3
        | image:no.png[]
        | Security-Enhanced Linux (SELinux) must be enabled on all of the cluster members.
        | SElinux disabled

	```

   Results:

 
	![Checklist](readme/checklist.png)


6.- Generate document executing following instruction:

* English

```
# cd documentation
# asciidoctor-pdf -b pdf healthcheck.adoc -a lang=EN -o healthcheck-EN.pdf
```

* Spanish

```
# cd documentation
# asciidoctor-pdf -b pdf healthcheck.adoc -a lang=ES -o healthcheck-ES.pdf
```

* Portuguese

```
# cd documentation
# asciidoctor-pdf -b pdf healthcheck.adoc -a lang=PT -o healthcheck-PT.pdf
```

7.- Check your documentation folder. You'll find your PDF 




