# (c) 2016, Matt Martz <matt@sivel.net>
#
# This file is part of Ansible
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.

# Make coding more python3-ish
# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import json
import os
import sys
import shutil
import glob
import unicodedata

# sys.path.append(os.path.dirname(os.path.abspath(__file__))+"/../python-routines/asciidoctorgenerator.py")
# from asciidoctorgenerator import *

from ansible.plugins.callback import CallbackBase


class CallbackModule(CallbackBase):
    # CALLBACK_VERSION = 2.0
    # CALLBACK_TYPE = 'stdout'
    # CALLBACK_NAME = 'json'

    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'notification'
    CALLBACK_NAME = 'json-custom-ES'
    CALLBACK_NEEDS_WHITELIST = True
    # PATH_REPORT=os.path.dirname(os.path.abspath(__file__));

    def __init__(self, display=None):
        super(CallbackModule, self).__init__(display)
        self.results = []

    def _new_play(self, play):
        return {
            'play': {
                'name': play.name,
                'id': str(play._uuid)
            },
            'tasks': []
        }

    def _new_task(self, task):
        return {
            'task': {
                'name': task.name,
                'id': str(task._uuid)
            },
            'hosts': {}
        }

    def v2_playbook_on_play_start(self, play):
        self.results.append(self._new_play(play))

    def v2_playbook_on_task_start(self, task, is_conditional):
        self.results[-1]['tasks'].append(self._new_task(task))

    def v2_runner_on_ok(self, result, **kwargs):
        host = result._host
        self.results[-1]['tasks'][-1]['hosts'][host.name] = result._result

    def v2_playbook_on_stats(self, stats):
        """Display info about playbook statistics"""

        hosts = sorted(stats.processed.keys())

        summary = {}
        for h in hosts:
            s = stats.summarize(h)
            summary[h] = s

        output = {
            'plays': self.results,
            'stats': summary
        }

        print(json.dumps(output, indent=4, sort_keys=True))
        facts_location=os.path.dirname(os.path.abspath(__file__))+ "/../documentation/";
        facts_file=facts_location+"play-data.json"
        if not os.path.exists(facts_location):
            os.makedirs(facts_location)
        with open(facts_file, "w") as text_file:
            text_file.write(json.dumps(output, indent=4, sort_keys=True))
        self.asciidoctorgenerator(output)

    def asciidoctorgenerator(self, facts):
        print("Starting generate-asciidoc()")
        # facts_file=os.path.dirname(os.path.abspath(__file__))+ "/../documentation/test.json"
        result_folder=os.path.dirname(os.path.abspath(__file__))+ "/../documentation/ES/";

        checklist_data = dict()
        checklist_file_list = []
        checklist_data_list = []

        print("Deleting old files in chapters/appendix")
        for f in glob.glob(result_folder+"chapters/appendix/data/*"):
            os.remove(f)
        for f in glob.glob(result_folder+"chapters/appendix/checklists/*"):
            os.remove(f)
        print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print(type(facts))
        print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print(facts)
        print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        for item in facts['plays']:
            if "nodoc" not in item['play']['name']:
                # Creating a new file
                item_play_name=self.translate_text(item['play']['name'])
                fname=result_folder+"chapters/appendix/data/"+item_play_name+"-"+item['play']["id"]+".adoc"
                checklist_data_list.append("data/"+item_play_name+"-"+item['play']["id"]+".adoc");

                with open(fname, "w") as text_file:
                    self.printLine(text_file, "=== "  + item_play_name +" - Datos de Soporte")
                    #self.printLine(text_file, ". Play: "    +item_play_name)
                    #self.printLine(text_file, ". Play ID: " +item['play']['id'])
                    #self.printLine(text_file, ". Size: "+   str(len(item['tasks'])))

                    self.printLine(text_file,"\n ");
                    tasksName = list()
                    for task in item['tasks']:
                        if task['task']['name'] and "nodoc" not in str(task['task']['name']):
                            tasksName.append(task['task']['name'])
                            self.printLine(text_file,"==== "+ self.translate_text(task['task']['name']))
                            #print("=============")
                    	    #print(task['hosts'])
                            #print("=============")
                            #print("=============")
                            for host,invhost in task['hosts'].items():
                                print("=============================================================================")
                                print(invhost)
                                print("=============================================================================")
                                if "invocation" in invhost:
                                    raw_cmd_tmp =invhost['invocation']['module_args']
                                    if '_raw_params' in raw_cmd_tmp:
                                        raw_cmd =invhost['invocation']['module_args']['_raw_params']
                                        print("======RAW CMD=======")
                                        print(raw_cmd)
                                        print("======FIN RAW CMD=======")
                                        #self.printLine(text_file,"."+host+": "+invhost['invocation']['module_name'])
                                        #self.printLine(text_file,"."+host+": "+invhost['invocation']['module_args']['_raw_params'])
                                        self.printLine(text_file,".Host: "+host)
                                        if "cat {}" not in raw_cmd:
                                            self.printLine(text_file,"[source,bash]")
                                            self.printLine(text_file,"----")
                                            self.printLine(text_file,"$ "+ str(raw_cmd))
                                            if invhost['stdout_lines']:
                                                for line in invhost['stdout_lines']:
                                                    self.printLine(text_file, str(line.encode('utf-8')))
                                            else:
                                                self.printLine(text_file, invhost['stderr'])
                                            self.printLine(text_file,"----")
                                            self.printLine(text_file,"\n ");
                                        else:
                                            self.printLine(text_file,"[source,bash]")
                                            self.printLine(text_file,"----")
                                            self.printLine(text_file,"$ "+ str(raw_cmd))
                                            self.printLine(text_file,"----")
                                            self.printLine(text_file,"\n ");
                                            #seccion de archivos
                                            self.printLine(text_file,"====")
                                            if invhost['stdout_lines']:
                                                for line in invhost['stdout_lines']:
                                                    self.printLine(text_file, str(line))
                                            else:
                                                self.printLine(text_file, invhost['stderr'])
                                            self.printLine(text_file,"\n ");
                                else:
                                    if "results" in invhost:
                                        print(len(invhost['results']))
                                        results_tmp =invhost['results']
                                        print(results_tmp)
                                        self.printLine(text_file,"[source,bash]")
                                        for result in results_tmp:
                                            self.printLine(text_file,"----")
                                            print(result['cmd'])
                                            self.printLine(text_file, result['cmd']);
                                            print("*********** LARGO STDOUT*****************")
                                            print(len(result['stdout_lines']))
                                            print("*****************************************")
                                            if len(result['stdout_lines']) > 1:
                                                multi_stdout=result['stdout_lines']
                                                print(multi_stdout)
                                                print("****************************************************************************")
                                                for rs in multi_stdout:
                                                    if "#" not in rs and "sh" not in rs:
                                                        rs=self.remove_accents(rs)
                                                        self.printLine(text_file, str(rs));
                                            elif len(result['stdout_lines']) == 1:
                                                 print(result['stdout_lines'][0])
                                                 self.printLine(text_file, result['stdout_lines'][0]);
                                            else:
                                                 print("********** NO DATA **********")
                                                 self.printLine(text_file, "********** NO DATA **********");
                                            self.printLine(text_file,"----")
                                        self.printLine(text_file,"\n ");

                #fin seccion de archivos
                print("-----------------------------------")
                print("INICIANDO CHECKLIST")
                print("-----------------------------------")
                print(tasksName)
                if "nodoc" not in tasksName:
                    checklist_data[item['play']['name']] = tasksName;
                    checklist_folder=result_folder+"chapters/appendix/checklists/"
                    print(checklist_data)
                    print("----------------------------------------------")
                    for key, value in checklist_data.items():
                        if "nodoc" not in key:
                            print("=============")
                            print(key)
                            print("=============")
                            print(value)
                            key=self.translate_text(key)
                            chkfilename=checklist_folder+"anexo_"+key+".adoc"
                            checklist_file_list.append("checklists/anexo_"+key+".adoc")

                            print(checklist_file_list)
                            with open(chkfilename, "w") as chkfile:
                                self.printLine(chkfile,"=== "+ key +" - Checklist" )
                                self.printLine(chkfile,"\n")
                                self.printLine(chkfile,"====")
                                self.printLine(chkfile,".Checklist "+key)
                                self.printLine(chkfile,"//[width=\"100%\", cols=\"^1,^1,6,16\", frame=\"topbot\",options=\"header\"]")
                                self.printLine(chkfile,"[width=\"100%\", cols=\"^1,^1,6,16\", frame=\"topbot\",options=\"header\"]")
                                self.printLine(chkfile,"|======================")
                                self.printLine(chkfile,"|#       \n|  \n| Item    \n| Comentarios")
                                self.printLine(chkfile,"\n")
                                cont = 0
                                for taskItem in value:
                                    if "nodoc" not in str(taskItem):
                                        cont = cont + 1
                            	        # self.printLine(chkfile, ""+str(taskItem))
                            	        # print str.replace("is", "was")
                                        self.printLine(chkfile,"| "+str(cont))
                                        self.printLine(chkfile,"| image:warn.png[]")
                                        self.printLine(chkfile,"| "+self.translate_text(str(taskItem).replace("|", "\|")))
                                        self.printLine(chkfile,"| Debe completarse despues del trabajo de analisis de los datos recopilados.")
                                        self.printLine(chkfile,"\n")
                                self.printLine(chkfile,"|======================")
                                self.printLine(chkfile,"====")


        ## Data Files creating process
        fname=result_folder+"chapters/appendix/appendix_checklist.adoc"
        with open(fname, "w") as chkfile:
            self.printLine(chkfile,"== Revision del Cluster Red Hat Openshift")
            for item in set(checklist_file_list):
                if "INIT" not in item:
                    self.printLine(chkfile,"\n")
                    self.printLine(chkfile,"include::"+item+"[]")
                # checklist_file_list.append("checklists/anexo_"+key+".adoc")
        fname=result_folder+"chapters/appendix/appendix_data.adoc"
        with open(fname, "w") as chkfile:
            self.printLine(chkfile,"<<<  ")
            for item in set(checklist_data_list):
                if "INIT" not in item:
                    self.printLine(chkfile,"\n")
                    self.printLine(chkfile,"include::"+item+"[]")

    def str_unicode(self,text):
        try:
            text = unicode(text, 'utf-8')
        except TypeError:
            return text

    def printLine(self,file, texto):
        print("PRINT LINE: "+ texto)
        file.write(texto+"\n");

    def remove_accents(self,input_str):
        nfkd_form = unicodedata.normalize('NFKD', input_str)
        only_ascii = nfkd_form.encode('ASCII', 'ignore')
        return only_ascii

    def translate_text(self, text):
        if text == "Kubeadmin user must have been removed":text="El usuario de Kubeadmin debe haber sido eliminado"
        if text == "The installation must be covered by active RH subscriptions to be supported.":text="La instalacion debe contar con suscripciones de Red Hat activas"
        if text == "A shared network must exist between the masters and the nodes.":text="Debe existir una red compartida entre los maestros y los nodos."
        if text == "Security-Enhanced Linux (SELinux) must be enabled on all of the cluster members.":text="Linux con seguridad mejorada (SELinux) debe estar habilitado en todos los miembros del cluster."
        if text == "Each host in the cluster must be configured to resolve hostnames from your DNS server.":text="Cada host en el cluster debe estar configurado para resolver los nombres de host de su servidor DNS."
        if text == "NetworkManager is required on the nodes in order to populate dnsmasq with the DNS IP addresses.":text="Se requiere NetworkManager en los nodos para completar dnsmasq con las direcciones IP de DNS."
        if text == "Proxy must allow the communication between the cluster nodes and masters.":text="El proxy debe permitir la comunicacion entre los nodos del cluster y los maestros."
        if text == "Time synchronization | NTP":text="Sincronizacion horaria | NTP"
        if text == "Cluster operators review":text="Revision de los Operators del cluster"
        if text == "Number of pods per node is < 250 (for OpenShift 4.2) and < 500 (for OpenShift 4.3+)":text="La cantidad de pods por nodo es <250 (para OpenShift 4.2) y <500 (para OpenShift 4.3+)"
        if text == "Number of namespaces < 10,000":text="Cantidad de namespaces < 10,000"
        if text == "Number of pods pre namespace < 25,000":text="Cantidad de pods por namespace < 25,000"
        if text == "Number of services < 10,000":text="Cantidad de servicios < 10,000"
        if text == "Number of pods < 150,000":text="Cantidad de pods < 150,000"
        if text == "Number of deployments per namespace < 2,000":text="Cantidad de despliegues por namespace < 2,000"
        if text == "Three control plane, or master, machines":text="Tres maquinas de control o master"
        if text == "Masters must run RHCOS":text="Los nodos master deben ejecutar RHCOS"
        if text == "Each master must have at least 4vCPUs":text="Cada nodo master debe tener al menos 4vCPU"
        if text == "Each master must have at least 16GB of RAM":text="Cada nodo master debe tener al menos 16 GB de RAM"
        if text == "Each master requires 120GB of disk space":text="Cada nodo master requiere 120GB de espacio en disco"
        if text == "RHCOS or RHEL 7.6 or later must be installed.":text="Sistema Operativo debe ser RHCOS o RHEL 7.6 o superior"
        if text == "Each compute node must have at least 2vCPU":text="Cada nodo de computo debe tener a lo menos 2vCPU"
        if text == "Each compute node must have at least 8GB of RAM":text="Cada nodo de computo debe tener a lo menos 8GB de RAM"
        if text == "Each compute requires 120GB of disk spac":text="Cada nodo de computo necesita 120GB de espacio en disco"
        if text == "Master Nodes Supportability Review":text="Revision de Soportabilidad sobre nodos master"
        if text == "Worker Nodes Supportability Review":text="Revision de Soportabilidad sobre nodos worker"
        if text == "General Supportability Review":text="Revision General de soportabilidad"
        if text == "General Best Practices Review":text="Revision General de Buenas Practicas"
        if text == "Master Nodes Best Practices Review":text="Revision de Buenas Practicas sobre nodos master"
        if text == "Verify connectivity to external registry URLs":text="Verifique la conectividad a las URL de registry externas"
        if text == "Verify connectivity to external telemetry URL":text="Verifique la conectividad a la URL de telemetria externa"
        return text


    v2_runner_on_failed = v2_runner_on_ok
    v2_runner_on_unreachable = v2_runner_on_ok
    v2_runner_on_skipped = v2_runner_on_ok
