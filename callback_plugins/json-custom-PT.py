# (c) 2016, Matt Martz <matt@sivel.net>
#
# This file is part of Ansible
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.

# Make coding more python3-ish
# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import json
import os
import sys
import shutil
import glob
import unicodedata

# sys.path.append(os.path.dirname(os.path.abspath(__file__))+"/../python-routines/asciidoctorgenerator.py")
# from asciidoctorgenerator import *

from ansible.plugins.callback import CallbackBase


class CallbackModule(CallbackBase):
    # CALLBACK_VERSION = 2.0
    # CALLBACK_TYPE = 'stdout'
    # CALLBACK_NAME = 'json'

    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'notification'
    CALLBACK_NAME = 'json-custom-PT'
    CALLBACK_NEEDS_WHITELIST = True
    # PATH_REPORT=os.path.dirname(os.path.abspath(__file__));

    def __init__(self, display=None):
        super(CallbackModule, self).__init__(display)
        self.results = []

    def _new_play(self, play):
        return {
            'play': {
                'name': play.name,
                'id': str(play._uuid)
            },
            'tasks': []
        }

    def _new_task(self, task):
        return {
            'task': {
                'name': task.name,
                'id': str(task._uuid)
            },
            'hosts': {}
        }

    def v2_playbook_on_play_start(self, play):
        self.results.append(self._new_play(play))

    def v2_playbook_on_task_start(self, task, is_conditional):
        self.results[-1]['tasks'].append(self._new_task(task))

    def v2_runner_on_ok(self, result, **kwargs):
        host = result._host
        self.results[-1]['tasks'][-1]['hosts'][host.name] = result._result

    def v2_playbook_on_stats(self, stats):
        """Display info about playbook statistics"""

        hosts = sorted(stats.processed.keys())

        summary = {}
        for h in hosts:
            s = stats.summarize(h)
            summary[h] = s

        output = {
            'plays': self.results,
            'stats': summary
        }

        print(json.dumps(output, indent=4, sort_keys=True))
        facts_location=os.path.dirname(os.path.abspath(__file__))+ "/../documentation/";
        facts_file=facts_location+"play-data.json"
        if not os.path.exists(facts_location):
            os.makedirs(facts_location)
        with open(facts_file, "w") as text_file:
            text_file.write(json.dumps(output, indent=4, sort_keys=True))
        self.asciidoctorgenerator(output)

    def asciidoctorgenerator(self, facts):
        print("Starting generate-asciidoc()")
        # facts_file=os.path.dirname(os.path.abspath(__file__))+ "/../documentation/test.json"
        result_folder=os.path.dirname(os.path.abspath(__file__))+ "/../documentation/PT/";

        checklist_data = dict()
        checklist_file_list = []
        checklist_data_list = []

        print("Deleting old files in chapters/appendix")
        for f in glob.glob(result_folder+"chapters/appendix/data/*"):
            os.remove(f)
        for f in glob.glob(result_folder+"chapters/appendix/checklists/*"):
            os.remove(f)
        print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print(type(facts))
        print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print(facts)
        print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        for item in facts['plays']:
            if "nodoc" not in item['play']['name']:
                # Creating a new file
                item_play_name=self.translate_text(item['play']['name'])
                fname=result_folder+"chapters/appendix/data/"+item_play_name+"-"+item['play']["id"]+".adoc"
                checklist_data_list.append("data/"+item_play_name+"-"+item['play']["id"]+".adoc");

                with open(fname, "w") as text_file:
                    self.printLine(text_file, "=== "  + item_play_name +" - Dados de suporte")
                    #self.printLine(text_file, ". Play: "    +item_play_name)
                    #self.printLine(text_file, ". Play ID: " +item['play']['id'])
                    #self.printLine(text_file, ". Size: "+   str(len(item['tasks'])))

                    self.printLine(text_file,"\n ");
                    tasksName = list()
                    for task in item['tasks']:
                        if task['task']['name'] and "nodoc" not in str(task['task']['name']):
                            tasksName.append(task['task']['name'])
                            self.printLine(text_file,"==== "+ self.translate_text(task['task']['name']))
                            #print("=============")
                    	    #print(task['hosts'])
                            #print("=============")
                            #print("=============")
                            for host,invhost in task['hosts'].items():
                                print("=============================================================================")
                                print(invhost)
                                print("=============================================================================")
                                if "invocation" in invhost:
                                    raw_cmd_tmp =invhost['invocation']['module_args']
                                    if '_raw_params' in raw_cmd_tmp:
                                        raw_cmd =invhost['invocation']['module_args']['_raw_params']
                                        print("======RAW CMD=======")
                                        print(raw_cmd)
                                        print("======FIN RAW CMD=======")
                                        #self.printLine(text_file,"."+host+": "+invhost['invocation']['module_name'])
                                        #self.printLine(text_file,"."+host+": "+invhost['invocation']['module_args']['_raw_params'])
                                        self.printLine(text_file,".Host: "+host)
                                        if "cat {}" not in raw_cmd:
                                            self.printLine(text_file,"[source,bash]")
                                            self.printLine(text_file,"----")
                                            self.printLine(text_file,"$ "+ str(raw_cmd))
                                            if invhost['stdout_lines']:
                                                for line in invhost['stdout_lines']:
                                                    self.printLine(text_file, str(line.encode('utf-8')))
                                            else:
                                                self.printLine(text_file, invhost['stderr'])
                                            self.printLine(text_file,"----")
                                            self.printLine(text_file,"\n ");
                                        else:
                                            self.printLine(text_file,"[source,bash]")
                                            self.printLine(text_file,"----")
                                            self.printLine(text_file,"$ "+ str(raw_cmd))
                                            self.printLine(text_file,"----")
                                            self.printLine(text_file,"\n ");
                                            #seccion de archivos
                                            self.printLine(text_file,"====")
                                            if invhost['stdout_lines']:
                                                for line in invhost['stdout_lines']:
                                                    self.printLine(text_file, str(line))
                                            else:
                                                self.printLine(text_file, invhost['stderr'])
                                            self.printLine(text_file,"\n ");
                                else:
                                    if "results" in invhost:
                                        print(len(invhost['results']))
                                        results_tmp =invhost['results']
                                        print(results_tmp)
                                        self.printLine(text_file,"[source,bash]")
                                        for result in results_tmp:
                                            self.printLine(text_file,"----")
                                            print(result['cmd'])
                                            self.printLine(text_file, result['cmd']);
                                            print("*********** LARGO STDOUT*****************")
                                            print(len(result['stdout_lines']))
                                            print("*****************************************")
                                            if len(result['stdout_lines']) > 1:
                                                multi_stdout=result['stdout_lines']
                                                print(multi_stdout)
                                                print("****************************************************************************")
                                                for rs in multi_stdout:
                                                    if "#" not in rs and "sh" not in rs:
                                                        rs=self.remove_accents(rs)
                                                        #rs=rs.replace("+","-")
                                                        #rs=rs.replace("\n","\\n")
                                                        print("ANTES DE IR AL FILE")
                                                        self.printLine(text_file, str(rs));
                                                        print("POST FILE")
                                            elif len(result['stdout_lines']) == 1:
                                                print(result['stdout_lines'][0])
                                                self.printLine(text_file, result['stdout_lines'][0]);
                                            else:
                                                print("************ SIN DATA RECOLECTADA ********************")
                                                self.printLine(text_file,"********* SIN DATA RECOLECTADA **********");
                                            self.printLine(text_file,"----")
                                        self.printLine(text_file,"\n ");

                #fin seccion de archivos
                print("-----------------------------------")
                print("INICIANDO A LISTA DE VERIFICACAO")
                print("-----------------------------------")
                print(tasksName)
                if "nodoc" not in tasksName:
                    checklist_data[item['play']['name']] = tasksName;
                    checklist_folder=result_folder+"chapters/appendix/checklists/"
                    print(checklist_data)
                    print("----------------------------------------------")
                    for key, value in checklist_data.items():
                        if "nodoc" not in key:
                            print("=============")
                            print(key)
                            print("=============")
                            print(value)
                            key=self.translate_text(key)
                            chkfilename=checklist_folder+"anexo_"+key+".adoc"
                            checklist_file_list.append("checklists/anexo_"+key+".adoc")

                            print(checklist_file_list)
                            with open(chkfilename, "w") as chkfile:
                                self.printLine(chkfile,"=== "+ key +" - Lista de verificacao")
                                self.printLine(chkfile,"\n")
                                self.printLine(chkfile,"====")
                                self.printLine(chkfile,".Checklist "+key)
                                self.printLine(chkfile,"//[width=\"100%\", cols=\"^1,^1,6,16\", frame=\"topbot\",options=\"header\"]")
                                self.printLine(chkfile,"[width=\"100%\", cols=\"^1,^1,6,16\", frame=\"topbot\",options=\"header\"]")
                                self.printLine(chkfile,"|======================")
                                self.printLine(chkfile,"|#       \n|  \n| Item    \n| Comentarios")
                                self.printLine(chkfile,"\n")
                                cont = 0
                                for taskItem in value:
                                    if "nodoc" not in str(taskItem):
                                        cont = cont + 1
                            	        # self.printLine(chkfile, ""+str(taskItem))
                            	        # print str.replace("is", "was")
                                        self.printLine(chkfile,"| "+str(cont))
                                        self.printLine(chkfile,"| image:warn.png[]")
                                        self.printLine(chkfile,"| "+self.translate_text(str(taskItem).replace("|", "\|")))
                                        self.printLine(chkfile,"| Debe completarse despues del trabajo de analisis de los datos recopilados.")
                                        self.printLine(chkfile,"\n")
                                self.printLine(chkfile,"|======================")
                                self.printLine(chkfile,"====")


        ## Data Files creating process
        fname=result_folder+"chapters/appendix/appendix_checklist.adoc"
        with open(fname, "w") as chkfile:
            self.printLine(chkfile,"== Revisao do Red Hat Openshift Cluster")
            for item in set(checklist_file_list):
                if "INIT" not in item:
                    self.printLine(chkfile,"\n")
                    self.printLine(chkfile,"include::"+item+"[]")
                # checklist_file_list.append("checklists/anexo_"+key+".adoc")
        fname=result_folder+"chapters/appendix/appendix_data.adoc"
        with open(fname, "w") as chkfile:
            self.printLine(chkfile,"<<<  ")
            for item in set(checklist_data_list):
                if "INIT" not in item:
                    self.printLine(chkfile,"\n")
                    self.printLine(chkfile,"include::"+item+"[]")

    def str_unicode(self,text):
        try:
            text = unicode(text, 'utf-8')
        except TypeError:
            return text

    def printLine(self,file, texto):
        print("PRINT LINE: "+ texto)
        file.write(texto+"\n");

    def remove_accents(self,input_str):
        nfkd_form = unicodedata.normalize('NFKD', input_str)
        only_ascii = nfkd_form.encode('ASCII', 'ignore')
        return only_ascii


    def translate_text(self, text):
        if text == "Kubeadmin user must have been removed":text="O usuario Kubeadmin deve ter sido removido"
        if text == "The installation must be covered by active RH subscriptions to be supported.":text="A instalacao deve ser coberta por assinaturas de RH ativas para ser suportada."
        if text == "A shared network must exist between the masters and the nodes.":text="Uma rede compartilhada deve existir entre os mestres e os nos."
        if text == "Security-Enhanced Linux (SELinux) must be enabled on all of the cluster members.":text="O Linux com seguranca aprimorada (SELinux) deve estar ativado em todos os membros do cluster."
        if text == "Each host in the cluster must be configured to resolve hostnames from your DNS server.":text="Cada host no cluster deve ser configurado para resolver nomes de host do seu servidor DNS."
        if text == "NetworkManager is required on the nodes in order to populate dnsmasq with the DNS IP addresses.":text="O NetworkManager e necessario nos nos para preencher o dnsmasq com os enderecos IP do DNS."
        if text == "Proxy must allow the communication between the cluster nodes and masters.":text="O proxy deve permitir a comunicacao entre os nos e mestres do cluster."
        if text == "Time synchronization | NTP":text="Sincronizacao de tempo | NTP"
        if text == "Cluster operators review":text="Revisao de operadores de cluster"
        if text == "Number of pods per node is < 250 (for OpenShift 4.2) and < 500 (for OpenShift 4.3+)":text="O numero de pods por no e <250 (para o OpenShift 4.2) e <500 (para o OpenShift 4.3 ou superior)"
        if text == "Number of namespaces < 10,000":text="Numero de namespaces <10.000"
        if text == "Number of pods pre namespace < 25,000":text="Numero de pods antes do espaco para nome <25.000"
        if text == "Number of services < 10,000":text="Numero de servicos <10.000"
        if text == "Number of pods < 150,000":text="Numero de pods <150.000"
        if text == "Number of deployments per namespace < 2,000":text="Numero de implantacoes por espaco de nome <2.000"
        if text == "Three control plane, or master, machines":text="Tres maquinas de plano de controle ou mestre"
        if text == "Masters must run RHCOS":text="Os mestres devem executar o RHCOS"
        if text == "Each master must have at least 4vCPUs":text="Cada mestre deve ter pelo menos 4vCPUs"
        if text == "Each master must have at least 16GB of RAM":text="Cada mestre deve ter pelo menos 16 GB de RAM"
        if text == "Each master requires 120GB of disk space":text="Cada mestre requer 120 GB de espaco em disco"
        if text == "RHCOS or RHEL 7.6 or later must be installed.":text="O RHCOS ou RHEL 7.6 ou posterior deve estar instalado."
        if text == "Each compute node must have at least 2vCPU":text="Cada no de computacao deve ter pelo menos 2vCPU"
        if text == "Each compute node must have at least 8GB of RAM":text="Cada no de computacao deve ter pelo menos 8 GB de RAM"
        if text == "Each compute requires 120GB of disk space":text="Cada computacao requer 120 GB de espaco em disco"
        if text == "Master Nodes Supportability Review":text="Revisao da capacidade de suporte dos nos principais"
        if text == "Worker Nodes Supportability Review":text="Revisao da capacidade de suporte dos nos do trabalhador"
        if text == "General Supportability Review":text="Revisao geral da capacidade de suporte"
        if text == "General Best Practices Review":text="Revisao geral das melhores praticas"
        if text == "Master Nodes Best Practices Review":text="Revisao de praticas recomendadas dos nos principais"
        if text == "Verify connectivity to external registry URLs":text="Verifique a conectividade com URLs de registry externos"
        if text == "Verify connectivity to external telemetry URL":text="Verifique a conectividade com o URL de telemetria externa"
        return text


    v2_runner_on_failed = v2_runner_on_ok
    v2_runner_on_unreachable = v2_runner_on_ok
    v2_runner_on_skipped = v2_runner_on_ok
